@extends('layouts.public.template')

@section('content')
<div class="android-wear-section" style="height: 250px;"></div>
<div class="section website-content">
    <div class="section section-center">
        <h3 class="mdl-typography--font-light mdl-typography--display-1-color-contrast">Download the Public Network Analyser</h3>
        <p>
            <a class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" target="_blank" href="https://github.com/sktan/public-network-analyser/archive/master.zip">
                <i class="fa fa-download"></i> Download Here
            </a>
        </p>
        <p>
            Once downloaded, unzip the file and browse to the file directory (for example)
            <pre>D:\Downloads\public-network-analyser-master</pre>
            You will then need to run this command to install it's dependencies
            <pre>pip install -r requirements.txt</pre>
            Once installed, you can run the script and paste the JSON output <a href="/analyse">here</a> for analysis. <br>
            Rename example_recon.ini to recon.ini <br>
            Modify the recon.ini config file
            <pre>python recon.py</pre>
        </p>
        <h3 class="mdl-typography--font-light mdl-typography--display-1-color-contrast">Prefer Pure CLI?</h3>
        <p>Use the commands below (unix based)!</p>
        <pre>git clone https://github.com/sktan/public-network-analyser.git
cd public-network-analyser
pip3 install -r requirements.txt
mv example_recon.ini recon.ini
vi recon.ini
python3 recon.py</pre>
        <p>Once run, paste the JSON output <a href="/analyse">here</a> for analysis.</p>
    </div>
</div>
@endsection()