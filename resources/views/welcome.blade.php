@extends('layouts.public.template')

@section('content')
<div class="android-wear-section">
    <div class="android-wear-band">
        <div class="android-wear-band-text">
            <div class="mdl-typography--display-2 mdl-typography--font-thin">Public Network Analyser</div>
            <p class="mdl-typography--headline mdl-typography--font-thin">
                This lightweight tool gathers and analyses statistics about the current network you are on.
            </p>
            <p>
                <a class="mdl-typography--font-regular mdl-typography--text-uppercase android-alt-link" href="">
                    Learn More<i class="material-icons">chevron_right</i>
                </a>
            </p>
        </div>
    </div>
</div>

<div class="android-customized-section">
    <div class="android-customized-section-text">
        <div class="mdl-typography--font-light mdl-typography--display-1-color-contrast">Compatibility?</div>
        <p class="mdl-typography--font-light">
            This tool runs on any operating system with Python installed including Windows, Linux and Mac OS X.
            <br>
            <a href="/download" class="android-link mdl-typography--font-light">Click me to get started</a>
        </p>
    </div>
    <div class="android-customized-section-image"></div>
</div>
@endsection()