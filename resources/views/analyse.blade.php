@extends('layouts.public.template')

@section('content')
<div class="android-wear-section" style="height: 250px;"></div>
<div class="section website-content">
    <div class="section section-center">
        <h3 class="mdl-typography--font-light mdl-typography--display-1-color-contrast">Anylse your JSON Output</h3>
        @if (count($errors) > 0)
        <dialog class="mdl-dialog" style="z-index: 9999;">
            <h4 class="mdl-dialog__title">Submission Failed</h4>
            <div class="mdl-dialog__content">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
            <div class="mdl-dialog__actions">
                <button type="button" class="mdl-button close">OK</button>
            </div>
        </dialog>
        <script>
            var dialog = document.querySelector('dialog');
            var showDialogButton = document.querySelector('#show-dialog');
            if (! dialog.showModal) {
                dialogPolyfill.registerDialog(dialog);
            }
            dialog.querySelector('.close').addEventListener('click', function() {
                dialog.close();
            });
            
            dialog.showModal();
        </script>
        @endif
        <form method="POST">
            {{ csrf_field() }}           
            <div class="mdl-textfield mdl-js-textfield" style="width: 100%;">
                <textarea class="mdl-textfield__input" name="json_output" type="text" rows= "10" required></textarea>
                <label class="mdl-textfield__label">JSON Output...</label>
            </div>
            
            <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored">
                <i class="fa fa-upload"></i> Upload Network Data
            </button>
        </form>
    </div>
</div>
@endsection()