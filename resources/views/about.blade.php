@extends('layouts.public.template')

@section('content')
<div class="android-wear-section" style="height: 250px;"></div>
<div class="section website-content">
    <div class="section section-center">
        <h3 class="mdl-typography--font-light mdl-typography--display-1-color-contrast">About the Public Network Analyser</h3>
        <p>
            This tool was created for a Network Security class as my individual "Major Project". 
            It's designed to run with minimal requirements with 4 requirements:
            <ol>
                <li> A working computer </li>
                <li> A network connection </li>
                <li> Python 3 </li>
                <li> Nmap </li>
            </ol>
        </p>
        <p>
            It's designed to use several metrics to determine your network's usability, security and reliability.
            These metrics include:
            <ul>
                <li> Network speeds </li>
                <li> Captive portal </li>
                <li> Network restrictions </li>
            </ul>
        </p>
        <p>
            How are the metrics scored? <br>
            Several metrics are collected and rated of which the lowest rating is used in the overview.
        </p>
        <p>
            Want to get started? Visit the <a href="/download">download page now!</a>
        </p>
        <p>
            <a class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" target="_blank" href="https://github.com/sktan/public-network-analyser">
                <i class="fa fa-github"></i> View on Github
            </a>
        </p>
    </div>
</div>
@endsection()