@extends('layouts.public.template')

@section('content')
<div class="android-wear-section" style="height: 250px;"></div>
<div class="section website-content">
    <div class="section section-center">
        <h3 class="mdl-typography--font-light mdl-typography--display-1-color-contrast">
            Analysis for {{ $network_data['network_interfaces']['external_ip'] }} 
        </h3>
        @if($analysis->completed == false)
        <div id="p2" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
        <p>
            We are currently awaiting for the analysis of this network to complete. <br>
            The page will automatically refresh on completion. <br>
        </p>
        @else
        <div class="demo-card-wide mdl-card mdl-shadow--2dp" style="width: 100%;">
            <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">General Analysis</h2>
            </div>
            @php
            $general_score = 100;
            foreach($results['scores'] as $key => $result) {
                if($result['result'] < $general_score) {
                    $general_score = $result['result'];
                }
            }
            @endphp
            <div class="mdl-card__supporting-text">
                ISP: {{ $results['isp'] }} <br>
                Detected Router: {{ $results['router'] }} <br>
                Network Rating: {{ score($general_score) }}
            </div>
            <div class="mdl-card__actions mdl-card--border">
                <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" id="btn_detailed">
                    See Detailed 
                </a>
            </div>
        </div>
        </br>
        <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" style="width: 100%; display: none;" id="tbl_detailed">
            <thead>
                <tr>
                    <th class="mdl-data-table__cell--non-numeric">Metric</th>
                    <th>Raw</th>
                    <th>Rating</th>
                </tr>
            </thead>
            <tbody>
                @foreach($results['scores'] as $key => $result)
                <tr>
                    <td class="mdl-data-table__cell--non-numeric">{{ $result['name'] }}</td>
                    <td>
                        {{ $result['raw'] }}
                    </td>
                    <td>
                        <button id="result_{{ $key }}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" style="height: 25px; line-height: 25px;">
                            <small>{{ score($result['result']) }}</small>
                        </button>

                        <div class="mdl-tooltip" for="result_{{ $key }}" style="max-width: 500px;">
                            {{ $result['reason'] }}
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @endif
    </div>
</div>
@endsection()

@section('custom_js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
@if($analysis->completed == false)
<script>
    function api_timer() {
        $.get("{{ route('api.analysis', $analysis) }}", function( data ) {
            if(data.completed) {
                window.location.reload(false);
            }
        });
    }

    setInterval(api_timer, 10000);
</script>
@else
<script>
    $("#btn_detailed").click(function() {
        $("#tbl_detailed").show();
    });
</script>
@endif
@endsection