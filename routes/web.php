<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home page route
Route::get('/', function () {
    return view('welcome');
});

// Download page route
Route::get('/download', function () {
    return view('download');
});

// Analyse form GET / POST route
Route::get('/analyse', "AnalyseController@getForm");
Route::post('/analyse', "AnalyseController@postForm");

// Analysis output route
Route::get('/analysis/{analysis}', "AnalyseController@getAnalysis")->name('analysis')
    ->where('analysis', '[0-9a-zA-Z]+');

// About page route
Route::get('/about', function () {
    return view('about');
});
