<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Analysis extends Model
{
    public $table = "analysis";
    //
    public function getRouteKeyName() {
        return 'hash';
    }
}
