<?php

function score( $mark ){
    $scores = [
        100 => 'A+',
        90 => 'A',
        82 => 'B',
        75 => 'C+',
        65 => 'C',
        40 => 'D',
        0 => 'F',
    ];

    foreach($scores as $score => $value) {
        if($mark >= $score)  {
            return $value;
        }
    }
}
