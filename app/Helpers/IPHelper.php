<?php

namespace App\Helpers;

use Symfony\Component\Process\Process;

class IPHelper
{
    /**
     * Takes a raw bps and converts it to a human readable format
     *
     * @return string
     */
    public static function format_speed($raw_bps) {
        $fmt = ['b/s', 'Kb/s', 'Mb/s', 'Gb/s'];
        $index = 0;
        $speed = $raw_bps;
        while ($speed > 1024) {
            $index += 1;
            $speed /= 1024;
        }
        return sprintf("%0.2f %s", $speed, $fmt[$index]);
    }
}