<?php

namespace App\Helpers;

use Symfony\Component\Process\Process;

class Nmap
{
    public static function portscan($ip, $ports = [22, 23, 80, 443]) {
        $process = new Process('nmap -p ' . implode(",", $ports) . ' ' . $ip);
        $process->run();

        $output = $process->getOutput();

        return $output;
    }
}