<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Analysis;
use Validator;
use App\Jobs\AnalyseNetwork;

class AnalyseController extends Controller
{
    //
    public function getForm(Request $request) {
        return View('analyse');
    }

    public function postForm(Request $request) {
        // Validates whether the submitted json string is valid
        Validator::extend('valid_network_json', function($attribute, $value, $parameters, $validator) {
            // Is this actually a json object?
            if($network_object = json_decode($value, true)) {
                // Is the version valid?
                if(empty($network_object['version']) || $network_object['version'] !== '2017-05-11')
                {
                    return false;
                }

                // Also validate the external IP
                if(!filter_var($network_object['network_interfaces']['external_ip'], FILTER_VALIDATE_IP)) {
                    return false;
                }
                
                return true;
            }

            return false;
        });

        $this->validate($request, [
            'json_output' => 'required|json|valid_network_json',
        ]);

        $analysis = new Analysis;
        $analysis->hash = hash("sha256", $request->json_output . openssl_random_pseudo_bytes(32));
        $analysis->data = $request->json_output;

        $analysis->save();

        dispatch(new AnalyseNetwork($analysis));

        return redirect()->route('analysis', $analysis->hash);
    }

    public function getAnalysis(Request $request, Analysis $analysis) {
        $network_data = json_decode($analysis->data, true);
        return View('layouts.analysis.' . $network_data['version'], [
            'analysis' => $analysis,
            'network_data' => $network_data,
            'results' => json_decode($analysis->results, true),
        ]);
    }

    public function getApi(Request $request, Analysis $analysis) {
        return [
            'completed' => $analysis->completed,
        ];
    }
}
