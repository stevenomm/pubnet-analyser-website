<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Analysis;
use App\Helpers\Nmap;
use GuzzleHttp\Client;
use App\Helpers\IPHelper;

class AnalyseNetwork implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $analysis;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Analysis $analysis)
    {
        //
        $this->analysis = $analysis;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $network = json_decode($this->analysis->data, true);

        // Mark job as completed
        $this->analysis->completed = true;
    
        $network_analysis = [];
        // General Information
        $network_analysis['isp'] = $this->get_isp($network['network_interfaces']['external_ip']);
        $network_analysis['router'] = str_replace(['\\r', '\r', '(', ')'], "", $network['router']);

        // Network Analysis
        $network_analysis['scores']['download_speeds'] = $this->check_download($network['internet_speeds']['download']);
        $network_analysis['scores']['upload_speeds'] = $this->check_upload($network['internet_speeds']['upload']);
        $network_analysis['scores']['ping'] = $this->check_ping($network['internet_speeds']['ping']);
        $network_analysis['scores']['open_ports'] = $this->port_scan($network['network_interfaces']['external_ip']);
        $network_analysis['scores']['dns_allowed'] = $this->dns_allowed($network['dns_test']);
        $network_analysis['scores']['mtfw_test'] = $this->check_mtfw($network['mtfw_hash']);
        $network_analysis['scores']['openvpn_allowed'] = $this->openvpn_allowed($network['openvpn_allowed']);
        $network_analysis['scores']['captive_portal'] = $this->captive_portal($network['captive_portal_required']);
        
        $this->analysis->results = json_encode($network_analysis);
        $this->analysis->save();
    }

    /**
     * Returns the ISP of an IP Address
     *
     * @return string
     */
    private function get_isp($ip) {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'http://ip-api.com/',
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]);

        $response = $client->request("GET", "/json/{$ip}");
        $response = json_decode($response->getBody(), true);

        return $response['isp'];
    }

    /**
     * Does a portscan for known management ports
     *
     * @return array
     */
    private function port_scan($ip) {
        $port_scan = Nmap::portscan($ip);

        $nmap_lines = explode($port_scan, '\n');
        $open_ports = [];
        foreach($nmap_lines as $line) {
            if(preg_match("/^[0-9]+\/[tcp|udp](.*)open(.*)/", $line)) {
                $matches = [];
                preg_match_all("[0-9]+", $line, $matches);
                $open_ports[] = $matches[0];
            }
        }

        $results = [];
        $results['name'] = "Open Management Ports";
        $results['raw'] = count($open_ports) > 0 ? "Yes" : "No";

        $results['ports'] = $open_ports;
        if(count($open_ports) == 0) {
            $results['result'] = count($open_ports) == 0 ? 100 : 60;
            $results['reason'] = "No known management ports open to public!";
        }
        else if(in_array(23, $open_ports)) {
            $results['result'] = 10;
            $results['reason'] = "TCP port 23 is open (known for telnet) on the router. Management access isn't secured!";
        }
        else if(in_array(80, $open_ports)) {
            $results['result'] = 10;
            $results['reason'] = "TCP port 80 is open (known for HTTP) on the router. Management access isn't secured!";
        }
        else if(count($open_ports) > 0) {
            $results['result'] = 70;
            $results['reason'] = "Secured / encrypted management access is open to the public.";
        }

        return $results;
    }
    
    /**
     * Checks whether OpenVPN is allowed on the network
     *
     * @return array
     */
    private function openvpn_allowed($allowed) {
        $results = [
            'name' => 'OpenVPN Connectivity',
            'result' => $allowed ? 100 : 65,
            'raw' => $allowed ? "Yes" : "No",
        ];

        if($allowed) {
            $results['reason'] = "An encrypted OpenVPN tunnel can be created to protect traffic.";
        }
        else {
            $results['reason'] = "OpenVPN seems to be blocked from this network.";
        }

        return $results;
    }

    /**
     * Checks whether OpenVPN is allowed on the network
     *
     * @return array
     */
    private function check_mtfw($mtfw_hash) {
        $results = [
            'name' => 'MITM Injection Test',
        ];

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'http://motherfuckingwebsite.com/',
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]);

        $response = $client->request("GET", "/");
        $changed = hash("sha256", $response->getBody()) != $mtfw_hash;

        if($changed) {
            $results['result'] = 0;
            $results['raw'] = "Difference Detected";
            $results['reason'] = "The request to a static website was changed in your request!";
        }
        else {
            $results['result'] = 100;
            $results['raw'] = "No Difference";
            $results['reason'] = "Request to a static website was clean.";
        }

        return $results;
    }
    /**
     * Checks whether 3rd Party DNS is allowed on the network
     *
     * @return array
     */
    private function dns_allowed($allowed) {
        $results = [
            'name' => '3rd Party DNS access',
            'result' => $allowed ? 100 : 45,
            'raw' => $allowed ? "Yes" : "No",
        ];

        if($allowed) {
            $results['reason'] = "You may use custom DNS servers.";
        }
        else {
            $results['reason'] = "3rd party DNS is blocked or intercepted.";
        }

        return $results;
    }
    /**
     * Checks whether Captive Portal is required
     *
     * @return array
     */
    private function captive_portal($required) {
        $results = [
            'name' => 'Captive Portal Requirement',
            'result' => $required == false ? 100 : 95,
            'raw' => $required ? "Yes" : "No",
        ];

        if($required == false) {
            $results['reason'] = "Network is free to use.";
        }
        else {
            $results['reason'] = "Access to network is limited via a captive portal.";
        }

        return $results;
    }

    /**
     * Checks whether the download speeds are "usable"
     *
     * @return array
     */
    private function check_download($speed) {
        // Convert bps to mbps
        $mbps = $speed / 1024 / 1024;

        $results = [
            'name' => 'Download Speeds',
            'raw' => IPHelper::format_speed($speed),
        ];

        if ($mbps >= 24) {
            $results['reason'] = "Internet meets / exceeds ADSL2+ speeds.";
            $results['result'] = 100;
        }
        else if ($mbps >= 16) {
            $results['reason'] = "Internet is faster than ADSL.";
            $results['result'] = 95;
        }
        else if ($mbps >= 12) {
            $results['reason'] = "Internet is somewhat faster than ADSL.";
            $results['result'] = 85;
        }
        else if ($mbps >= 8) {
            $results['reason'] = "Internet is enough for HD Videos.";
            $results['result'] = 75;
        }
        else if ($mbps >= 5) {
            $results['reason'] = "Internet is just enough for HD videos.";
            $results['result'] = 65;
        }
        else if ($mbps >= 3) {
            $results['reason'] = "Internet is just enough for SD videos.";
            $results['result'] = 40;
        }
        else if ($mbps >= 1.5) {
            $results['reason'] = "Internet is just best for text websites.";
            $results['result'] = 25;
        }
        else if ($mbps >= 0) {
            $results['reason'] = "Internet would be barely usable for modern websites.";
            $results['result'] = 0;
        }

        return $results;
    }

     /**
     * Checks whether the upload speeds are "usable"
     *
     * @return array
     */
    private function check_upload($speed) {
        // Convert bps to mbps
        $mbps = $speed / 1024 / 1024;

        $results = [
            'name' => 'Upload Speeds',
            'raw' => IPHelper::format_speed($speed),
        ];

        if ($mbps >= 20) {
            $results['reason'] = "Upload speeds are very usable for streaming and uploading.";
            $results['result'] = 100;
        }
        else if ($mbps >= 10) {
            $results['reason'] = "Upload speeds are usable for streaming and uploading.";
            $results['result'] = 95;
        }
        else if ($mbps >= 5) {
            $results['reason'] = "Upload speeds are very fast.";
            $results['result'] = 85;
        }
        else if ($mbps >= 2) {
            $results['reason'] = "Upload speeds above ADSL2+ standard.";
            $results['result'] = 75;
        }
        else if ($mbps >= 1) {
            $results['reason'] = "Upload speeds standard.";
            $results['result'] = 65;
        }
        else if ($mbps >= 0.5) {
            $results['reason'] = "Upload speeds fairly slow.";
            $results['result'] = 40;
        }
        else if ($mbps >= 0) {
            $results['reason'] = "Upload speeds may affect the internet usability.";
            $results['result'] = 0;
        }

        return $results;
    }

    /**
     * Checks whether the ping latency is suitable
     *
     * @return array
     */
    private function check_ping($ping) {
        $results = [
            'name' => 'Ping Latency',
            'raw' => round($ping),
        ];

        if ($ping <= 5) {
            $results['reason'] = "Latency is super low.";
            $results['result'] = 100;
        }
        else if ($ping <= 10) {
            $results['reason'] = "Latency is low.";
            $results['result'] = 95;
        }
        else if ($ping <= 15) {
            $results['reason'] = "Latency is low.";
            $results['result'] = 85;
        }
        else if ($ping <= 20) {
            $results['reason'] = "Latency is fairly low.";
            $results['result'] = 75;
        }
        else if ($ping <= 50) {
            $results['reason'] = "Latency is slightly high.";
            $results['result'] = 65;
        }
        else if ($ping <= 80) {
            $results['reason'] = "Latency is pretty high.";
            $results['result'] = 40;
        }
        else if ($ping >= 80) {
            $results['reason'] = "Latency is quite high.";
            $results['result'] = 0;
        }

        return $results;
    }
}
